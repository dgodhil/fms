package marennikov.fms.services;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import marennikov.fms.csvmodel.CsvFms;
import marennikov.fms.exceptions.ApiException;
import marennikov.fms.exceptions.ResourceNotFoundException;
import marennikov.fms.model.FmsInfo;
import marennikov.fms.repositories.FmsInfoRepository;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.UnzipParameters;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Service
public class FmsInfoServiceImpl implements FmsInfoService {

    private final String fmsUrl = "http://webzato.com/fms/fms_structure_10012018.zip";
    private final String fileSource = "downloads/fms_structure_10012018.zip";
    private final String unZipDirectory = "downloads/unzip/";
    private final String file = "fms_structure_10012018.csv";
    private final Supplier<ResourceNotFoundException> resourceNotFoundExceptionSupplier = () -> new ResourceNotFoundException("Fms not found");

    private FmsInfoRepository fmsInfoRepository;

    @Autowired
    public void setFmsInfoRepository(FmsInfoRepository fmsInfoRepository) {
        this.fmsInfoRepository = fmsInfoRepository;
    }

    @Override
    public boolean downloadFmsAndParseList() {
        List<FmsInfo> savedFms = fmsInfoRepository.findAll();

        if (downloadFms() && unzipFile()) {
            Set<CsvFms> csvFms = parseFmsFromCsv();
            if (csvFms != null && !csvFms.isEmpty()) {
                if (savedFms.isEmpty()) {
                    fmsInfoRepository.save(csvFms.stream().map(FmsInfo::new).collect(Collectors.toList()));
                } else {
                    csvFms.removeIf(fms -> savedFms.stream().anyMatch(sFms -> sFms.getName().equals(fms.name) && sFms.getCode().equals(fms.code)));
                    fmsInfoRepository.save(csvFms.stream().map(FmsInfo::new).collect(Collectors.toList()));
                }
                removeDownloadedFiles();
            } else {
                removeDownloadedFiles();
                throw new ApiException("Ошибка скачивания данных ФМС");
            }
        }
        return true;
    }

    @Override
    public List<FmsInfo> getFmsByCode(String code) {
        if (StringUtils.isBlank(code)) {
            throw new ApiException("FMS code is null", HttpStatus.BAD_REQUEST);
        }
        List<FmsInfo> result = fmsInfoRepository.getAllByCode(code);
        if (result.isEmpty()) {
            throw new ApiException("Fms not found");
        }
        return result;
    }

    @Override
    public List<FmsInfo> getAll() {
        return fmsInfoRepository.findAll();
    }

    private boolean downloadFms() {
        URL url;
        try {
            url = new URL(fmsUrl);
        } catch (MalformedURLException e) {
            return false;
        }

        HttpURLConnection con;
        try {
            con = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            return false;
        }

        try (InputStream stream = con.getInputStream()) {
            Files.copy(stream, Paths.get(fileSource), REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private boolean unzipFile() {
        try {
            ZipFile zipFile = new ZipFile(fileSource);
            UnzipParameters param = new UnzipParameters();
            param.setIgnoreAllFileAttributes(true);
            zipFile.extractAll(unZipDirectory, param);
        } catch (ZipException e) {
            return false;
        }
        return new File(unZipDirectory, file).exists();
    }

    private Set<CsvFms> parseFmsFromCsv() {
        Reader reader;
        try {
            reader = Files.newBufferedReader(Paths.get(unZipDirectory + file), Charset.forName("windows-1251"));
        } catch (IOException e) {
            return null;
        }

        CsvToBean csvToBean = new CsvToBeanBuilder(reader)
                .withType(CsvFms.class)
                .withIgnoreLeadingWhiteSpace(true)
                .build();
        Set<CsvFms> result = new HashSet<>(csvToBean.parse());
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void removeDownloadedFiles() {
        File fileZip = new File(fileSource);
        if (!fileZip.delete()) {
            System.out.println("Delete operation is failed.");
        }

        File fileCsv = new File(unZipDirectory + file);
        if (!fileCsv.delete()) {
            System.out.println("Delete operation is failed.");
        }

    }

}
