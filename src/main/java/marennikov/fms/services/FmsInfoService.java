package marennikov.fms.services;

import marennikov.fms.model.FmsInfo;

import java.util.List;

public interface FmsInfoService {

    boolean downloadFmsAndParseList();

    List<FmsInfo> getFmsByCode(String code);

    List<FmsInfo> getAll();


}
