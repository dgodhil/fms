package marennikov.fms.controllers;

import marennikov.fms.dto.InformDto;
import marennikov.fms.model.FmsInfo;
import marennikov.fms.services.FmsInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/fms")
public class ApiFmsController {

    private FmsInfoService fmsInfoService;

    @Autowired
    public void setFmsInfoService(FmsInfoService fmsInfoService) {
        this.fmsInfoService = fmsInfoService;
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/{code}"})
    public List<FmsInfo> get(@PathVariable String code) {
        return fmsInfoService.getFmsByCode(code);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/download")
    public ResponseEntity<InformDto> downloadFmsList() {
        fmsInfoService.downloadFmsAndParseList();
        return ResponseEntity.ok(new InformDto("Данные успешно скачены"));
    }
}
