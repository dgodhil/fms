package marennikov.fms.csvmodel;

import com.opencsv.bean.CsvBindByName;

public class CsvFms {
    @CsvBindByName(column = "Полное наименование")
    public String name;

    @CsvBindByName(column = "Код")
    public String code;

}
