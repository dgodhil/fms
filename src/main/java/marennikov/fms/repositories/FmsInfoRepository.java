package marennikov.fms.repositories;


import marennikov.fms.model.FmsInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FmsInfoRepository extends JpaRepository<FmsInfo, Long> {

    Optional<FmsInfo> getById(long id);

    List<FmsInfo> getAllByCode(String code);
}
