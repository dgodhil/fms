package marennikov.fms.services;

import marennikov.fms.model.FmsInfo;
import marennikov.fms.repositories.FmsInfoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@TestPropertySource("/application.yml")
@SpringBootTest
@RunWith(SpringRunner.class)
public class FmsInfoServiceTest {

    private FmsInfoService fmsInfoService;
    private FmsInfoRepository fmsInfoRepository;

    @Autowired
    public void setFmsInfoService(FmsInfoService fmsInfoService) {
        this.fmsInfoService = fmsInfoService;
    }

    @Autowired
    public void setFmsInfoRepository(FmsInfoRepository fmsInfoRepository) {
        this.fmsInfoRepository = fmsInfoRepository;
    }

    @Before
    public void before() {
        fmsInfoRepository.deleteAll();
    }

    @Test
    public void downloadFmsTest() {
        assertTrue(fmsInfoRepository.count() == 0);
        assertTrue(fmsInfoService.downloadFmsAndParseList());
        assertTrue(fmsInfoRepository.count() > 0);
    }

    @Test
    public void getFmsByCodeTest() {
        FmsInfo fms = new FmsInfo("TestName", "TestCode");
        fmsInfoRepository.save(fms);

        List<FmsInfo> fmsInfos = fmsInfoService.getFmsByCode(fms.getCode());

        assertEquals(fms.getId(), fmsInfos.get(0).getId());
        assertEquals(fms.getName(), fmsInfos.get(0).getName());
        assertEquals(fms.getCode(), fmsInfos.get(0).getCode());
    }

    @Test
    public void getAllTest() {
        List<FmsInfo> fmsInfos = fmsInfoService.getAll();
        assertEquals(0, fmsInfos.size());

        FmsInfo fms1 = new FmsInfo("TestName 1", "TestCode 1");
        fmsInfoRepository.save(fms1);
        FmsInfo fms2 = new FmsInfo("TestName 2", "TestCode 2");
        fmsInfoRepository.save(fms2);
        FmsInfo fms3 = new FmsInfo("TestName 3", "TestCode 3");
        fmsInfoRepository.save(fms3);

        fmsInfos = fmsInfoService.getAll();
        assertEquals(3, fmsInfos.size());
    }
}