package marennikov.fms.controllers;

import marennikov.fms.model.FmsInfo;
import marennikov.fms.repositories.FmsInfoRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@TestPropertySource("/application.yml")
@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ApiFmsControllerTest {
    private MediaType mediaTypeJson = new MediaType(MediaType.APPLICATION_JSON, Charset.defaultCharset());
    private MockMvc mockMvc;
    private HttpMessageConverter messageConverter;
    private WebApplicationContext webApplicationContext;
    private FmsInfoRepository fmsInfoRepository;

    @Autowired
    public void setFmsInfoRepository(FmsInfoRepository fmsInfoRepository) {
        this.fmsInfoRepository = fmsInfoRepository;
    }

    @Autowired
    private void setWebApplicationContext(WebApplicationContext webApplicationContext) {
        this.webApplicationContext = webApplicationContext;
    }

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.messageConverter = Arrays.stream(converters)
                .filter(c -> c instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.messageConverter);
    }


    @Before
    public void before() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        fmsInfoRepository.deleteAll();
    }

    @Test
    public void getByCodeTest() throws Exception {
        FmsInfo fms = new FmsInfo("TestName", "TestCode");
        fmsInfoRepository.save(fms);

        mockMvc.perform(get("/api/fms/" + fms.getCode()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andExpect(jsonPath("$.[0].id", is(fms.getId().intValue())))
                .andExpect(jsonPath("$.[0].name", is(fms.getName())))
                .andExpect(jsonPath("$.[0].code", is(fms.getCode())));
    }

    @Test
    public void downloadFmsListTest() throws Exception {
        mockMvc.perform(get("/api/fms/download"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(mediaTypeJson))
                .andExpect(jsonPath("$.message", is("Данные успешно скачены")));

    }

}